package com.test.mq.consumer.queue;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * Created by xingyuzhu on 2018/3/23.
 */
@Component
public class DoubleDirectionMessageConsumer {
    @JmsListener(destination = "double.queue")
    @SendTo("out.queue")
    public String receiveQueue(String message) {
        System.out.println("消息来啦：" + message);
        return "OK, 我收到了 " + message;
    }
}
