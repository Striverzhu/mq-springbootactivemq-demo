package com.test.mq.consumer.queue;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Created by xingyuzhu on 2018/3/23.
 */
@Component
public class SingleDirectionMessageConsumer {
    @JmsListener(destination = "single.queue")
    public void receiveMessage(String text) {
        System.out.println("消息来啦：" + text);
    }
}
