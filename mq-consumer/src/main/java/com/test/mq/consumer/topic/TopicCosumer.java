package com.test.mq.consumer.topic;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Created by xingyuzhu on 2018/3/23.
 */
@Component
public class TopicCosumer {
    @JmsListener(destination = "single.topic")
    public void receiveTopic(String message) {
        System.out.println(message);
    }
}
