package com.test.mq.job;

import com.test.mq.producer.MessageProducer;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.Destination;

/**
 * Created by xingyuzhu on 2018/3/23.
 */
@Component
public class ScheduleJob {
    @Autowired
    private MessageProducer producer;

    @Scheduled(fixedDelay = 2000)
    public void sendQueueMessage() {
        Destination destination = new ActiveMQQueue("single.queue");
        producer.sendMessage(destination, "lalalala...");
    }

    @Scheduled(fixedDelay = 3000)
    public void sendTopicMessage() {
        Destination destination = new ActiveMQTopic("single.topic");
        producer.sendMessage(destination, "i am topic message,hahahah...");
    }
}
