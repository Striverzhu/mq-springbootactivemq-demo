package com.test.mq;

import com.test.mq.producer.MessageProducer;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Destination;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MqProducerApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    private MessageProducer producer;

    @Test
    public void sendMessage() throws InterruptedException {
        Destination singleDestination = new ActiveMQQueue("single.queue");
        Destination doubleDestination = new ActiveMQQueue("double.queue");
        for (int i = 0; i < 10; i++) {
            producer.sendMessage(singleDestination, "i am sending single" + i);
            producer.sendMessage(doubleDestination, "i am sending double" + i);
        }
    }
}
